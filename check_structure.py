
import os

class Check_files():

    def __init__(self, folder_name):
        self.path = folder_name

    def is_file(self, addinfo = ""):
        return os.path.exists(f"{self.path}{addinfo}")

    def file_exists_info(self, state, addinfo = ""):
        info = f"MIEJSCE '/{self.path}{addinfo}' "

        if state == True:
            info += f"ISTNIEJE - JEST OK!\n"
        else:
            info += f"NIE ISTNIEJE - TRZEBA STWORZYC!\n"

        return info

    # def files_in_folder(self, addinfo = ""):
    #     l = list(os.listdir())
    #     print(l)