
from check_structure import *
from photos_processing import *
import glob
import os
import math
import time

def percent_display(progress_text, progress_value):

    spaces = (40 - len(progress_text)) * " "
    fill = int(progress_value / 100 * 50) * "|"
    empty = (50 - int(progress_value / 100 * 50)) * "-"

    return progress_text + spaces + "[" + fill + empty + "]"


if __name__ == '__main__':

    # initialization
    check_result = ""
    folder_name = input("Ktory folder potraktowac algorytmem? Pamietaj o uzupelnieniu plikow w folderach! ")

    #base_photo parameters
    base_photo_width = 0
    base_photo_height = 0

    # miniatures parameters
    create_mini = 0
    scale = 1
    mini_width = 40
    mini_height = 30
    max_image_times = 1 # nie moze byc mniejsze niz ilosc miniaturek

    # create objects
    structure = Check_files(folder_name)

    # check exsists main folder
    result = structure.is_file()
    check_result += structure.file_exists_info(result)

    # check exsists /photos
    if result:
        path = "/photos"
        result = structure.is_file(path)
        check_result += structure.file_exists_info(result, path)

    # check exsists base photo
    if result:
        path = "/base_photo.jpg"
        result = structure.is_file(path)
        check_result += structure.file_exists_info(result, path)

    # create instance of class base_photo_processing
    base_images = photos_preprocessing(folder_name, mini_width, mini_height, scale)

    # get dimensions base photo
    if result:
        base_photo_width, base_photo_height = base_images.get_dimensions()
        check_result += f"WYMIARY OBRAZU BAZOWEGO: {base_photo_width}x{base_photo_height}\n"

    # check dimensions base photo
    if result:
        result = base_images.equal_with_mini(base_photo_width, base_photo_height)
        check_result += f"WYMIARY OBRAZU BAZOWEGO >= WYMIARY MINIATUR: {result}\n"

    # if exsists old temporary base photo...
    if result:
        path = "/temporary_base_photo.jpg"
        result = structure.is_file(path)
        check_result += structure.file_exists_info(result, path)

        if result:
            check_result += "---------- USUNE!\n\n"

            # ...delete old temporary base photo:
            os.remove(f"{folder_name}{path}")
            result = structure.is_file(path)
            check_result += structure.file_exists_info(result, path)

        result = not result

    # create new temporary base photo
    if result:
        base_images.adjust_base_photo_size()

        path = "/temporary_base_photo.jpg"
        result = structure.is_file(path)
        check_result += structure.file_exists_info(result, path)
        if result:
            check_result += "---------- STWORZYLEM!\n\n"

    # if exsists old white canvas...
    if result:
        path = "/white_canvas.jpg"
        result = structure.is_file(path)
        check_result += structure.file_exists_info(result, path)

        if result:
            check_result += "---------- USUNE!\n\n"

            # ...delete old white canvas:
            os.remove(f"{folder_name}{path}")
            result = structure.is_file(path)
            check_result += structure.file_exists_info(result, path)

        result = not result

    # create new white canvas
    if result:
        base_images.create_white_canvas()

        path = "/white_canvas.jpg"
        result = structure.is_file(path)
        check_result += structure.file_exists_info(result, path)
        if result:
            check_result += "---------- STWORZYLEM!\n\n"

    # check exsists any files in /photos (not delete):
    if result:
        pass

    # if exsists any old files in /miniatures...
    if result:
        pass

    # ...delete any old files in /miniatures:
    if result:
        pass

    # create new miniatures
    if result and create_mini:
        counter = 0
        for photofile in glob.glob(f"{folder_name}/photos/*.jpg"):
            print(photofile, counter)
            if base_images.create_miniature(photofile, counter):
                counter += 1

        check_result += f"STWORZYLEM {counter} MINIATUREK!\n"
        result = bool(counter)

    # check exsists any files in miniatures
    # after created new
    if result:
        pass

    # -------------------------------------------------------------------
    # ---- main algorithm -----------------------------------------------
    # -------------------------------------------------------------------

    t1 = time.time_ns()

    # 1 zaladowanie miniaturek jako wartosci liczbowe
    if result:
        list_minifiles = list(os.listdir(f"{folder_name}/miniatures/"))
        dict_minifiles = {}
        dict_minifiles_status = {}

        count = 0
        length = len(list_minifiles)

        print(f"1 - 0/{length} - 0.00%")

        for minifile in list_minifiles:
            count += 1
            dict_minifiles[minifile] = base_images.convert_mini_to_num(f"{folder_name}/miniatures/{minifile}")
            dict_minifiles_status[minifile] = max_image_times

            print(f"{1} - {minifile} - {count}/{length} - {(count / length) * 100:.2f}%")

        # r, g, b = dict_minifiles["13.jpg"][0][0]
        # print(dict_minifiles["13.jpg"][0][0])
        # print(r, g, b)

    t2 = time.time_ns()

    # 2 podzielnie obrazu roboczego na miniaturki i zaladowanie ich jako wartosci liczbowe
    if result:

        K = int(base_images.work_width / mini_width)
        L = int(base_images.work_height / mini_height)

        print(f"2 - 0/{K}, 0/{L} - 0.00%")

        # create matrix K x L
        a = []
        for i in range(K):
            row = []
            for j in range(L):
                row.append(base_images.convert_part_to_num(i, j, f"{folder_name}/temporary_base_photo.jpg"))

                print(f"2 - {i + 1}/{K}, {j + 1}/{L} - {((i * L + (j + 1)) / (K * L)) * 100:.2f}%")

            a.append(row)

    # r, g, b = a[1][1][0][0]
    # print(r, g, b)

    t3 = time.time_ns()

    # 3 selekcja wlasciwego obrazu na wlasciwie miejsce - glowny algorytm
    if result:

        print(percent_display(f"3 - 0/{K}, 0/{L} - 0.00%", 0))

        const_d = math.sqrt(pow(255 - 0, 2) + pow(255 - 0, 2) + pow(255 - 0, 2)) * mini_width * mini_height
        # quality = 0

        for i in range(K):
            for j in range(L):

                min_d = const_d

                for minifile in list_minifiles:

                    if dict_minifiles_status[minifile] == 0:
                        continue

                    sum_d = 0
                    next = False

                    for x in range(mini_width):
                        for y in range(mini_height):
                            r1, g1, b1 = dict_minifiles[minifile][x][y]
                            r2, g2, b2 = a[i][j][x][y]

                            d = math.sqrt(pow(r2 - r1, 2) + pow(g2 - g1, 2) + pow(b2 - b1, 2))
                            sum_d += d

                            if sum_d > min_d:   #nie wychodzi z 2 petli
                                next = True
                                break

                        if next:
                            break

                    if next:
                        continue

                    if sum_d <= min_d:
                        min_d = sum_d
                        good_mini = minifile
                        # dict_minifiles_status[minifile] -= 1
                        # quality_tmp = min_d / const_d

                a[i][j] = good_mini
                dict_minifiles_status[good_mini] -= 1
                # quality += quality_tmp

                percents = ((i*L + (j+1)) / (K*L)) * 100
                print(percent_display(f"3 - {i+1}/{K}, {j+1}/{L} - {percents:.2f}%", percents))


    t4 = time.time_ns()


    # 4 wklejenie wybranych miniatur na ich pozycje
    if result:
        base_images.paste_miniatures(K, L, a, f"{folder_name}/white_canvas.jpg")

    if result:
        pass

    print("\n\n")
    print(check_result)

    print(f"{(t2 - t1) / 1_000_000_000}  -   1 zaladowanie miniaturek jako wartosci liczbowe")
    print(f"{(t3 - t2) / 1_000_000_000}  -   2 podzielnie obrazu roboczego na miniaturki i zaladowanie ich jako wartosci liczbowe")
    print(f"{(t4 - t3) / 1_000_000_000}  -   3 selekcja wlasciwego obrazu na wlasciwie miejsce - glowny algorytm")
    print("----------------")
    print(f"{(t4 - t1) / 1_000_000_000}  -   SUMA CZASU OD pkt. 1 do 3")
    print("----------------")
    # print(f"Dopasowanie: {100 - (quality / (K * L) * 100):.2f}%")