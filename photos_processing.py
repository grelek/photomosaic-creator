
from PIL import Image


class photos_preprocessing():

    def __init__(self, folder_name, mini_width, mini_height, scale):
        self.path = folder_name
        self.scale = scale

        self.mini_width = mini_width
        self.mini_height = mini_height

        self.base_image = Image.open(f"{self.path}/base_photo.jpg")
        self.base_width = self.base_image.width
        self.base_height = self.base_image.height

        self.work_width = 0
        self.work_height = 0

    def equal_with_mini(self, width, height, scale = 1):
        return width >= self.mini_width * scale and height >= self.mini_height * scale

    def get_dimensions(self):
        return self.base_width, self.base_height

    def search_picture_center(self, big_width, small_width, big_height, small_height):
        x1 = int((big_width - small_width) / 2)
        x2 = x1 + small_width - 1
        y1 = int((big_height - small_height) / 2)
        y2 = y1 + small_height - 1

        return x1, x2, y1, y2

    def adjust_base_photo_size(self):

        self.work_width = int(self.base_width / self.mini_width) * self.mini_width
        self.work_height = int(self.base_height / self.mini_height) * self.mini_height

        x1, x2, y1, y2 = self.search_picture_center(self.base_width, self.work_width, self.base_height, self.work_height)

        tmp_base_photo = self.base_image.transform((self.work_width, self.work_height), Image.EXTENT, (x1, y1, x2, y2))
        tmp_base_photo.save(f"{self.path}/temporary_base_photo.jpg", "JPEG")

    def adjust_miniature(self, width, height, image):

        w_min = int(width / (self.mini_width * self.scale))
        h_min = int(height / (self.mini_height * self.scale))
        min_w_h = min(w_min, h_min)

        miniature_width = min_w_h * self.mini_width * self.scale
        miniature_height = min_w_h * self.mini_height * self.scale

        x1, x2, y1, y2 = self.search_picture_center(width, miniature_width, height, miniature_height)

        tmp_miniature_photo = image.transform((miniature_width, miniature_height), Image.EXTENT, (x1, y1, x2, y2))

        return tmp_miniature_photo

    def create_white_canvas(self):
        size = (self.work_width * self.scale, self.work_height * self.scale)

        white_image = Image.new("RGB", size, color=(255, 255, 255))
        white_image.save(f"{self.path}/white_canvas.jpg", "JPEG")

    def create_miniature(self, photo, count):
        bigfile = Image.open(photo)

        if self.equal_with_mini(bigfile.width, bigfile.height, self.scale):

            minifile = self.adjust_miniature(bigfile.width, bigfile.height, bigfile)

            minifile.thumbnail((self.mini_width * self.scale, self.mini_height * self.scale))
            minifile.save(f"{self.path}/miniatures/{count}.jpg", "JPEG")

            return True

        return False

    def convert_mini_to_num(self, photo):
        minifile = Image.open(photo)

        k_avg = self.scale * self.scale

        # create matrix M x N
        a = []
        for i in range(0, self.mini_width):
            row = []
            for j in range(0, self.mini_height):

                Ra, Ga, Ba = 0, 0, 0

                for m in range(i * self.scale, i * self.scale + self.scale):
                    for n in range(j * self.scale, j * self.scale + self.scale):
                        Rb, Gb, Bb = minifile.getpixel((i, j)) # tu chyba ma być m, n !!!!!!!!!!!!!!!!!!!
                        Ra += Rb
                        Ga += Gb
                        Ba += Bb

                Ra = int(Ra/k_avg)
                Ga = int(Ga/k_avg)
                Ba = int(Ba/k_avg)

                row.append((Ra, Ga, Ba))
                #row.append(minifile.getpixel((i, j)))

            a.append(row)

        return a

    def convert_part_to_num(self, M, N, photo):
        bigfile = Image.open(photo)

        # create matrix M x N
        a = []
        for i in range(M * self.mini_width, M * self.mini_width + self.mini_width):
            row = []
            for j in range(N * self.mini_height, N * self.mini_height + self.mini_height):
                row.append(bigfile.getpixel((i, j)))

            a.append(row)

        return a

    def paste_miniatures(self, M, N, data_matrix, photo):

        whitefile = Image.open(photo)

        for i in range(0, M):
            for j in range(0, N):
                mini = Image.open(f"{self.path}/miniatures/{data_matrix[i][j]}")
                whitefile.paste(mini, (i * self.mini_width * self.scale, j * self.mini_height * self.scale))

        whitefile.show()
        whitefile.save(f"{self.path}/result_image.jpg", "JPEG")